const CACHE_NAME = 'SW-002';
const toCache = [
    
    'js/script.js',
    'js/web.webmanifest',
    'js/register.js',
    'images/showcase/showcase-1.png',
    'images/showcase/showcase-2.png',
    'images/showcase/showcase-3.png',
    'images/showcase/showcase-4.png',
    'images/showcase/showcase-5.png',
    'images/showcase/showcase-6.png',
    'images/showcase/showcase-7.png',
    'images/showcase/showcase-8.png',
    'images/icon.png',
    'images/avater.png',
    'images/logo.png',
    'images/favicon.png',
    'images/watch.png',
    'images/watch-2.png',
    'images/call-to-action.jpg',
    'css/style.css',
    'css/style.css.map',
    'plugins/bootstrap/bootstrap.min.css',
    'plugins/bootstrap/bootstrap.min.js',
    'plugins/jquery/jquery.js',
    'plugins/slick/slick.min.js',
    'plugins/slick/slick.min.css',
    'plugins/themefisher-font/fonts/themefisher-font.eot',
    'plugins/themefisher-font/fonts/themefisher-font.svg',
    'plugins/themefisher-font/fonts/themefisher-font.ttf',
    'plugins/themefisher-font/fonts/themefisher-font.woff',
    'plugins/themefisher-font/themefisher-font.min.css',


];

self.addEventListener('install', function(event) {
    event.waitUntil(
        caches.open(CACHE_NAME)
        .then(function(cache) {
            return cache.addAll(toCache)
        })
        .then(self.skipWaiting())
    )
})

self.addEventListener('fetch', function(event) {
    event.respondWith(
        fetch(event.request)
        .catch(() => {
            return caches.open(CACHE_NAME)
            .then((cache) => {
                return cache.match(event.request)
            })
        })
    )
})

self.addEventListener('activate', function(event) {
    event.waitUntil(
        caches.keys()
        .then((keyList) => {
            return Promise.all(keyList.map((key) => {
                if (key !== CACHE_NAME) {
                    console.log('[ServiceWorker] Hapus cache lama', key)
                    return caches.delete(key)
                }
            }))
        })
        .then(() => self.clients.claim())
    )
})
